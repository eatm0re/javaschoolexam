package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;
import java.util.PriorityQueue;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int rows = calcRowsCount(inputNumbers.size());
        if (rows == 0) {
            return new int[0][0];
        }
        int[][] res = new int[rows][2 * rows - 1];
        PriorityQueue<Integer> pq = null;
        try {
            pq = new PriorityQueue<>(inputNumbers);
        } catch (NullPointerException e) {
            throw new CannotBuildPyramidException();
        }
        for (int i = 0; i < rows; i++) {
            int startPos = res[i].length / 2 - i;
            for (int j = 0; j <= i; j++) {
                res[i][startPos + 2*j] = pq.poll();
            }
        }
        return res;
    }

    private int rowsCountPreCalc(double n) {
        return (int) Math.round((-1 + Math.sqrt(1 + 8 * n)) / 2);
    }

    private int calcRowsCount(int n) {
        int rows = rowsCountPreCalc(n);
        if ((long) (1 + rows) * rows / 2 == n) {
            return rows;
        } else {
            throw new CannotBuildPyramidException();
        }
    }
}
