package com.tsystems.javaschool.tasks.zones;

import java.util.HashMap;
import java.util.List;

public class RouteChecker {


    /**
     * Checks whether required zones are connected with each other.
     * By connected we mean that there is a path from any zone to any zone from the requested list.
     *
     * Each zone from initial state may contain a list of it's neighbours. The link is defined as unidirectional,
     * but can be used as bidirectional.
     * For instance, if zone A is connected with B either:
     *  - A has link to B
     *  - OR B has a link to A
     *  - OR both of them have a link to each other
     *
     * @param zoneState current list of all available zones
     * @param requestedZoneIds zone IDs from request
     * @return true of zones are connected, false otherwise
     */
    public boolean checkRoute(List<Zone> zoneState, List<Integer> requestedZoneIds){
        HashMap<Integer, Zone> zones = new HashMap<>();
        for (Zone z : zoneState) {
            zones.put(z.getId(), z);
        }
        for (Integer id : requestedZoneIds) {
            Zone z = zones.get(id);
            if (z == null) {
                return false;
            }
            z.setComponent(new Component(z));
        }
        for (Integer id : requestedZoneIds) {
            Zone z = zones.get(id);
            for (Integer neighbourId : z.getNeighbours()) {
                Zone neighbour = zones.get(neighbourId);
                if (neighbour.getComponent() != null && z.getComponent() != neighbour.getComponent()) {
                    z.getComponent().merge(neighbour.getComponent());
                }
            }
        }
        return zones.get(requestedZoneIds.get(0)).getComponent().size() == requestedZoneIds.size();
    }
}
