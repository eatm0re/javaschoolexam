package com.tsystems.javaschool.tasks.zones;

import java.util.LinkedList;

public class Component {

    private final LinkedList<Zone> zones = new LinkedList<>();
    private int size;

    public Component(Zone z) {
        zones.add(z);
        size++;
    }

    public void merge(Component c) {
        zones.addAll(c.zones);
        size += c.size;
        for (Zone z : c.zones) {
            z.setComponent(this);
        }
    }

    public int size() {
        return size;
    }
}
